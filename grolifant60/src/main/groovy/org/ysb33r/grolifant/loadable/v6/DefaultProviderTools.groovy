/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.loadable.v6

import groovy.transform.CompileStatic
import groovy.transform.InheritConstructors
import org.gradle.api.provider.Provider

/**
 * Safely deal with Providers down to Gradle 6.0.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1
 */
@CompileStatic
@InheritConstructors
@SuppressWarnings('ClassNameSameAsSuperclass')
class DefaultProviderTools extends org.ysb33r.grolifant.loadable.v5.DefaultProviderTools {

    /**
     * Allow orElse functionality prior to Gradle 5.6.
     *
     * Returns a Provider whose value is the value of this provider, if present, otherwise the given default value.
     *
     * @param provider Original provider.
     * @param value The default value to use when this provider has no value.
     * @param < T >    Provider type.
     * @return Provider value or default value.
     *
     * @since 1.2
     */
    @Override
    public <T> Provider<T> orElse(Provider<T> provider, T value) {
        provider.orElse(value)
    }

    /**
     * Allow orElse functionality prior to Gradle 5.6.
     * Returns a Provider whose value is the value of this provider, if present, otherwise uses the value from the
     * given provider, if present.
     *
     * @param provider Original provider
     * @param elseProvider The provider whose value should be used when this provider has no value.
     * @param < T >    Provider type
     * @return Provider chain.
     *
     * @since 1.2
     */
    @Override
    public <T> Provider<T> orElse(Provider<T> provider, Provider<? extends T> elseProvider) {
        provider.orElse(elseProvider)
    }
}
