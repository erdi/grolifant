/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v4

import org.gradle.api.Project
import org.gradle.process.ExecResult
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.v4.ExtensionUtils
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.v4.exec.AbstractCommandExecSpec
import org.ysb33r.grolifant.api.v4.exec.ExecSpecInstantiator
import org.ysb33r.grolifant.api.v4.exec.ResolverFactoryRegistry
import spock.lang.Issue
import spock.lang.Specification

import java.nio.file.Path
import java.nio.file.attribute.PosixFilePermission

import static java.nio.file.Files.getPosixFilePermissions
import static java.nio.file.Files.setPosixFilePermissions
import static java.nio.file.attribute.PosixFilePermission.OWNER_EXECUTE

@SuppressWarnings(['UnnecessarySetter', 'LineLength'])
@Deprecated
class ExecProjectExtensionSpec extends Specification {

    public static final OperatingSystem OS = OperatingSystem.current()
    public static final File TESTDIST_DIR = new File(System.getProperty('COMPAT_TEST_RESOURCES_DIR') ?: 'src/gradleTest/runtimeCompatibility/src/test/resources').absoluteFile
    public static final String toolExt = OS.windows ? 'cmd' : 'sh'

    static
    // tag::example-exec-spec[]
    class GitExecSpec extends AbstractCommandExecSpec {
        GitExecSpec(ProjectOperations project) {
            super(project, new ResolverFactoryRegistry(project))
            setExecutable('git')
        }
    }
    // end::example-exec-spec[]

    Project project = ProjectBuilder.builder().build()
    ProjectOperations projectOperations

    void setup() {
        projectOperations = ProjectOperations.create(project)
    }

    void 'Add execution specification to project as extension'() {
        when:
        // tag::adding-extension[]
        ExtensionUtils.addExecProjectExtension('gitexec', projectOperations, { ProjectOperations po ->
            new GitExecSpec(po)
        } as ExecSpecInstantiator<GitExecSpec>) // <1>
        // end::adding-extension[]

        then:
        project.extensions.extraProperties.get('gitexec')
    }

    @Issue('https://gitlab.com/ysb33rOrg/grolifant/-/issues/66')
    void 'Run executable as extension'() {
        setup:
        File targetExecutable = new File(TESTDIST_DIR, "mycmd.${toolExt}")
        ExtensionUtils.addExecProjectExtension('myrunner', projectOperations, { ProjectOperations po ->
            new GitExecSpec(po)
        } as ExecSpecInstantiator<GitExecSpec>)

        OutputStream output = new ByteArrayOutputStream()
        setExecutePermission(targetExecutable)

        when:
        Closure configurator = {
            command 'install'
            standardOutput output
            executable targetExecutable
        }

        ExecResult result = project.myrunner configurator

        then:
        result.exitValue == 0
        output.toString().startsWith('install')
    }

    void setExecutePermission(File target) {
        if (!OS.windows) {
            Path path = target.toPath()
            Set<PosixFilePermission> perms = getPosixFilePermissions(path)
            perms.add(OWNER_EXECUTE)
            setPosixFilePermissions(path, perms)
        }
    }
}