/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.compatibility.v4

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

import static org.ysb33r.grolifant.api.v4.ConfigurationUtils.asConfigurations

@SuppressWarnings(['UnnecessaryBooleanExpression'])
class ConfigurationUtilsSpec extends Specification {

    void 'Can create configurations from a list of objects'() {
        given:
        Project project = ProjectBuilder.builder().build()
        def created = ['a', 'b', 'c', 'd'].collect {
            project.configurations.create(it)
        }

        def configs = [
            project.configurations.getByName('a'),
            'b',
            project.provider { -> project.configurations.getByName('c') },
            { -> 'd' }
        ]
        when:
        def resolved = asConfigurations(project, configs)

        then:
        resolved == created
    }
}