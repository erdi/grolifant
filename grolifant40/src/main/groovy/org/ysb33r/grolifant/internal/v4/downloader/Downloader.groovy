/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v4.downloader

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.logging.LogLevel
import org.gradle.wrapper.Download
import org.gradle.wrapper.IDownload
import org.ysb33r.grolifant.api.core.BaseProgressLogger
import org.ysb33r.grolifant.api.errors.UnsupportedConfigurationException

import java.lang.reflect.Constructor

import static org.ysb33r.grolifant.internal.core.Issue.ISSUE_URL

/**
 * @since 0.8
 */
@CompileStatic
@Slf4j
class Downloader {

    static final String INSTALLER_VERSION = '1.0'

    final IDownload downloader
    final BaseProgressLogger progressLogger

    @CompileDynamic
    @SuppressWarnings(['CouldBeSwitchStatement', 'UnnecessaryPackageReference'])
    static Downloader create(
        final String distributionName,
        LogLevel currentGradleLogLevel
    ) {
        boolean quiet = currentGradleLogLevel < LogLevel.INFO
        IDownload downloader
        BaseProgressLogger progressLogger

        Download.constructors.findResult { Constructor ctor ->
            if (ctor.parameterTypes == [org.gradle.api.logging.Logger, String, String] as Class[]) {
                progressLogger = new Progress(quiet)
                downloader = ctor.newInstance(log, distributionName, INSTALLER_VERSION)
            } else if (ctor.parameterTypes == [org.gradle.wrapper.Logger, String, String] as Class[]) {
                progressLogger = new Progress(quiet)
                downloader = ctor.newInstance(
                    org.gradle.wrapper.Logger.newInstance(true),
                    distributionName,
                    INSTALLER_VERSION
                )
            } else if (ctor.parameterTypes == [String, String] as Class[]) {
                progressLogger = new Progress(quiet)
                downloader = ctor.newInstance(distributionName, INSTALLER_VERSION)
            }
        }

        if (downloader == null) {
            throw new UnsupportedConfigurationException('Cannot create an appropriate downloader. ' +
                'This is probably due to a change in the Gradle API in a new release of Gradle. ' +
                "Please report this exception as an issue at ${ISSUE_URL}."
            )
        }

        new Downloader(downloader, progressLogger)
    }

    @Deprecated
    static Downloader create(final String distributionName, Project project) {
        create(distributionName, project.gradle.startParameter.logLevel)
    }

    private Downloader(IDownload d, BaseProgressLogger bpl) {
        this.downloader = d
        this.progressLogger = bpl
    }

    private static class Progress implements BaseProgressLogger {
        private final boolean quiet

        Progress(boolean quiet) {
            this.quiet = quiet
        }

        void log(String message) {
            if (!quiet) {
                println message
            }
        }
    }

}
