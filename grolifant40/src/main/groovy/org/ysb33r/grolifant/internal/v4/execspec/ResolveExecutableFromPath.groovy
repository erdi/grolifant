/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.v4.execspec

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.v4.exec.ResolvableExecutable
import org.ysb33r.grolifant.api.v4.exec.ResolvedExecutableFactory

/** Resolves an exe from a path.
 *
 * @since 0.3
 */
@CompileStatic
class ResolveExecutableFromPath implements ResolvedExecutableFactory {

    @Deprecated
    ResolveExecutableFromPath(Project project) {
        this.grolifant = ProjectOperations.create(project)
    }

    /**
     *
     * @param project Project Operations
     *
     * @since 1.0
     */
    ResolveExecutableFromPath(ProjectOperations project) {
        this.grolifant = project
    }

    /** Builds a path-located execution resolver.
     *
     * @param options Ignored.
     * @param lazyPath Lazy-evaluated path to use.
     * Anything that can be resolved by {@code ProjectOperations#file} is acceptable.
     * @return Resolved path to exe.
     */
    @Override
    ResolvableExecutable build(Map<String, Object> options, Object lazyPath) {
        new Resolver(lazyPath, grolifant)
    }

    private final ProjectOperations grolifant

    private static class Resolver implements ResolvableExecutable {
        Resolver(final Object lazyPath, final ProjectOperations grolifant) {
            this.lazyPath = lazyPath
            this.grolifant = grolifant
        }

        @Override
        File getExecutable() {
            grolifant.file(this.lazyPath)
        }

        @Override
        String toString() {
            executable.absolutePath
        }

        private final Object lazyPath
        private final ProjectOperations grolifant
    }
}
