/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.downloader;

import java.io.File;
import java.util.function.BiConsumer;

/**
 * Unpacks an artifact.
 *
 * @author Schalk W. Cronjé
 * @since 1.1
 */
public interface ArtifactUnpacker extends BiConsumer<File, File> {
    /**
     * Unpacks the source archive
     *
     * @param source  Source archive to unpack. Can also be a single non-archive file, in which case the
     *                implementation will perform a straight copy.
     * @param destDir Destination directory.
     */
    default void unpack(File source, File destDir) {
        accept(source, destDir);
    }
}
