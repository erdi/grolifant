/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.downloader

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Action
import org.gradle.api.Transformer
import org.gradle.api.file.CopySpec
import org.gradle.api.file.FileCopyDetails
import org.gradle.api.file.FileTree
import org.gradle.api.provider.Provider
import org.gradle.internal.os.OperatingSystem
import org.tukaani.xz.XZInputStream
import org.ysb33r.grolifant.api.core.CheckSumVerification
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.errors.ChecksumFailedException
import org.ysb33r.grolifant.api.errors.DistributionFailedException
import org.ysb33r.grolifant.api.v4.FileUtils
import org.ysb33r.grolifant.internal.v4.downloader.Downloader
import org.ysb33r.grolifant.internal.v4.msi.LessMSIUnpackerTool

import java.security.MessageDigest
import java.util.concurrent.Callable

import static org.ysb33r.grolifant.api.v4.UriUtils.safeUri

/** Common functionality to be able to download a SDK and use it within Gradle
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1
 */
@CompileStatic
@Slf4j
@SuppressWarnings('AbstractClassWithoutAbstractMethod')
abstract class AbstractDistributionInstaller implements DistributionInstaller {

    public static final boolean IS_WINDOWS = OperatingSystem.current().windows

    ArtifactRootVerification artifactRootVerification
    ArtifactUnpacker artifactUnpacker

    /** Set candidate name for SdkMan if the latter should be searched for installed versions
     *
     * @param sdkCandidateName SDK Candidate name. This is the same names that will be shown when
     *   running {@code sdk list candidates} on the script-line.
     */
    void setSdkManCandidateName(final String sdkCandidateName) {
        this.sdkManCandidateName = sdkCandidateName
    }

    /**
     * SDKman candidate name for distribution.
     *
     * @return Name of SDKman candidate. Can be {@code null}.
     */
    @Override
    String getSdkManCandidateName() {
        this.sdkManCandidateName
    }

    /** Add patterns for files to be marked exe,
     *
     * Calling this method multiple times simply appends for patterns
     * @param relPaths One or more ANT-stype include patterns
     */
    void addExecPattern(String... relPaths) {
        this.execPatterns.addAll(relPaths as List)
    }

    /** Set a checksum that needs to be verified against downloaded archive
     *
     * @param cs SHA-256 Hex-encoded checksum
     */
    void setChecksum(final String cs) {
        if (cs.length() != 64 || !(cs ==~ /[\p{Digit}\p{Alpha}]{64}/)) {
            throw new IllegalArgumentException('Not a valid SHA-256 checksum')
        }
        this.checksum = cs.toLowerCase()
    }

    /** Returns the location which is the top or home folder for a distribution.
     *
     *  This value is affected by {@link #setDownloadRoot(java.io.File)} and
     *  the parameters passed in during construction time.
     *
     * @return Location of the distribution.
     */
    Provider<File> getDistributionRoot(String version) {
        projectOperations.provider(new Callable<File>() {
            @Override
            File call() throws Exception {
                resolveDistributionLocationForVersion(version)
            }
        })
    }

    /** Sets a download root directory for the distribution.
     *
     * If not supplied the default is to use the Gradle User Home.
     * This method is provided for convenience and is mostly only used for testing
     * purposes.
     *
     * The folder will be created at download time if it does not exist.
     *
     * @param downloadRootDir Any writeable directory on the filesystem.
     */
    void setDownloadRoot(Object downloadRootDir) {
        this.downloadRoot = downloadRootDir
    }

    /**
     * Locates a file within the distribution
     *
     * @param version Version of distribution to search.
     * @param fileRelPath Relative path to the distribution root.
     * @return Location of the file in the distribution.
     */
    @Override
    Provider<File> getDistributionFile(String version, String fileRelPath) {
        projectOperations.map(getDistributionRoot(version)) { File file ->
            new File(file, fileRelPath)
        }
    }

    /**
     * Resolves distribution location by looking in various locations.
     *
     * @param version Version to resolve
     * @return Location or {@code null} if nothing could be found.
     */
    protected File resolveDistributionLocationForVersion(String version) {
        // tag::download_logic[]
        File location = locateDistributionInCustomLocation(version) // <1>

        if (location == null && sdkManCandidateName) { // <2>
            location = getDistFromSdkMan(version)
        }

        location ?: getDistFromCache(version) // <3>
        // end::download_logic[]
    }

    /** Attempts to locate distribution in the list of SdkMan candidates.
     *
     * @return Location of the distribution if found in the candidate area.
     */
    protected File getDistFromSdkMan(String version) {
        File sdkCandidate = new File(
            "${System.getProperty('user.home')}/.sdkman/${sdkManCandidateName}/${version}"
        )

        sdkCandidate.exists() && sdkCandidate.directory ? sdkCandidate : null
    }

    /** Creates a distribution if it does not exist already.
     *
     * @return Location of distribution
     */
    protected File getDistFromCache(String version) {
        URI distUri = uriFromVersion(version)

        new ArtifactDownloader(
            distUri,
            projectOperations.file(this.downloadRoot) ?: projectOperations.gradleUserHomeDir.get(),
            projectOperations.projectDir,
            basePath,
            this.artifactRootVerification,
            this.artifactUnpacker,
            checksum ? getCheckSumVerificationInstance(safeUri(distUri).toString()) : null
        ).getFromCache(
            "${distributionName}:${version}",
            projectOperations.offline,
            this.downloadWorker
        )
    }

    /**
     * Project operations that can used during downloading.
     *
     */
    protected final ProjectOperations projectOperations

    /**
     * Name of the distribution.
     *
     */
    protected final String distributionName

    /**
     * Creates setup for installing to a local cache.
     *
     * @param distributionName Descriptive name of the distribution
     * @param basePath Relative path below Gradle User Home to create cache for all version of this distribution type.
     * @param projectOperations Gradle project operations that this downloader can use.
     */
    protected AbstractDistributionInstaller(
        final String distributionName,
        final String basePath,
        final ProjectOperations projectOperations
    ) {
        this.distributionName = distributionName
        this.projectOperations = projectOperations
        this.basePath = basePath
        this.downloadWorker = Downloader.create(
            distributionName,
            projectOperations.gradleLogLevel
        )

        this.artifactRootVerification = this.&verifyDistributionRoot as ArtifactRootVerification
        this.artifactUnpacker = this.&unpack as ArtifactUnpacker
        this.downloadRoot = projectOperations.map(projectOperations.gradleUserHomeDir, { File it ->
            new File(it, basePath)
        } as Transformer<File, File>)
        this.tmpDir = projectOperations.buildDirDescendant("tmp/${this.class.name}")
    }

    /** Validates that the unpacked distribution is good.
     *
     * <p> The default implementation simply checks that only one directory should exist and then uses that.
     * You should override this method if your distribution in question does not follow the common practice of one
     * top-level directory.
     *
     * @param distDir Directory where distribution was unpacked to.
     * @return The directory where the real distribution now exists. In the default implementation it will be
     *   the single directory that exists below {@code distDir}.
     *
     * @throw {@link DistributionFailedException} if distribution failed to
     *   meet criteria.
     */
    protected File verifyDistributionRoot(final File distDir) {
        List<File> dirs = listDirs(distDir)
        if (dirs.empty) {
            throw new DistributionFailedException("${distributionName} " +
                'does not contain any directories. Expected to find exactly 1 directory.'
            )
        }
        if (dirs.size() != 1) {
            throw new DistributionFailedException(
                "${distributionName}  contains too many directories. " +
                    'Expected to find exactly 1 directory.'
            )
        }
        dirs[0]
    }

    /** Verifies the checksum (if provided) of a newly downloaded distribution archive.
     *
     * <p> Only SHA-256 is supported at this point in time.
     *
     * @param sourceUrl The URL/URI where it was downloaded from
     * @param localCompressedFile The location of the downloaded archive
     * @param expectedSum The expected checksum. Can be null in which case no checks will be performed.
     *
     * @throw {@link ChecksumFailedException} if the checksum did not match
     */
    protected void verifyDownloadChecksum(
        final String sourceUrl, final File localCompressedFile, final String expectedSum) {
        if (expectedSum != null) {
            String actualSum = calculateSha256Sum(localCompressedFile)
            if (this.checksum != actualSum) {
                localCompressedFile.delete()
                throw new ChecksumFailedException(
                    distributionName,
                    sourceUrl,
                    localCompressedFile,
                    expectedSum,
                    actualSum
                )
            }
        }
    }

    /** Provides a list of directories below an unpacked distribution
     *
     * @param distDir Unpacked distribution directory
     * @return List of directories. Can be empty is nothing was unpacked or only files exist within the
     *   supplied directory.
     */
    protected List<File> listDirs(File distDir) {
        FileUtils.listDirs(distDir)
    }

    /** Unpacks a downloaded archive.
     *
     * <p> The default implementation supports the following formats:
     *
     * <ul>
     *   <li>zip</li>
     *   <li>tar</li>
     *   <li>tar.gz & tgz</li>
     *   <li>tar.bz2 & tbz</li>
     *   <li>tar.xz</li>
     * </ul>
     *
     * <p> If you need MSI support you need to override this method and call out to the
     * provided {@link #unpackMSI} method yourself.
     *
     * @param srcArchive The location of the download archive
     * @param destDir The directory where the archive needs to be unpacked into
     */
    @CompileDynamic
    protected void unpack(final File srcArchive, final File destDir) {
        final FileTree archiveTree = compressedTree(srcArchive)
        final List<String> patterns = this.execPatterns

        final Action<FileCopyDetails> setExecMode = { FileCopyDetails fcd ->
            if (!fcd.directory) {
                fcd.mode = fcd.mode | 0111
            }
        }

        projectOperations.copy { CopySpec cs ->
            cs.with {
                from archiveTree
                into destDir

                if (!IS_WINDOWS && !patterns.empty) {
                    filesMatching(patterns, setExecMode)
                }
            }
        }
    }

    /** Provides the capability of unpacking an MSI file under Windows by calling out to {@code msiexec}.
     *
     * <p> {@code msiexec} will be located via the system search path.
     *
     * @param srcArchive The location of the download MSI
     * @param destDir The directory where the MSI needs to be unpacked into
     * @param env Environment to use. Can be null or empty in which case a default environment will be used
     */
    protected void unpackMSI(File srcArchive, File destDir, final Map<String, String> env) {
        if (IS_WINDOWS) {
            new LessMSIUnpackerTool(projectOperations).unpackMSI(srcArchive, destDir, env)
        } else {
            throw new DistributionFailedException('MSI unpacking is only supported under Windows')
        }
    }

    private String calculateSha256Sum(final File file) {
        file.withInputStream { InputStream content ->
            MessageDigest digest = MessageDigest.getInstance('SHA-256')
            content.eachByte(4096) { bytes, len -> digest.update(bytes, 0, len) }
            digest.digest().encodeHex().toString()
        }
    }

    private FileTree compressedTree(final File srcArchive) {
        final String name = srcArchive.name.toLowerCase()
        if (name.endsWith('.zip')) {
            return projectOperations.zipTree(srcArchive)
        } else if (name.endsWith('.tar')) {
            return projectOperations.tarTree(srcArchive)
        } else if (name.endsWith('.tar.gz') || name.endsWith('.tgz')) {
            return projectOperations.tarTree(projectOperations.gzipResource(srcArchive))
        } else if (name.endsWith('.tar.bz2') || name.endsWith('.tbz')) {
            return projectOperations.tarTree(projectOperations.bzip2Resource(srcArchive))
        } else if (name.endsWith('.tar.xz')) {
            File workDir = tmpDir.get()
            workDir.mkdirs()
            final File unpackedXZTar = File.createTempFile(
                srcArchive.name.replaceAll(~/.xz$/, ''),
                '$$$',
                workDir
            )
            unpackedXZTar.deleteOnExit()
            unpackedXZTar.withOutputStream { OutputStream xz ->
                srcArchive.withInputStream { tarXZ ->
                    new XZInputStream(tarXZ).withStream { strm ->
                        xz << strm
                    }
                }
            }
            return projectOperations.tarTree(unpackedXZTar)
        }

        throw new IllegalArgumentException("${name} is not a supported archive type")
    }

    private CheckSumVerification getCheckSumVerificationInstance(String textUri) {
        final String chk = checksum

        new CheckSumVerification() {
            @Override
            void verify(File downloadedTarget) {
                verifyDownloadChecksum(textUri, downloadedTarget, chk)
            }

            @Override
            String getChecksum() {
                chk
            }
        }
    }
    private String sdkManCandidateName
    private String checksum
    private Object downloadRoot

    private final List<String> execPatterns = []
    private final String basePath
    private final Downloader downloadWorker
    private final Provider<File> tmpDir
}
