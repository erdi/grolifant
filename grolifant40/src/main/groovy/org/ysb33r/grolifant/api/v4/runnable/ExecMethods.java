/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.runnable;

import org.gradle.api.Action;
import org.gradle.process.ExecResult;

/**
 * Adds interface to a {@link AbstractToolExtension} implementation, so that the spirit of the original
 * {@code exec} project extension can be maintained for a specific tool.
 *
 * @author Schalk W. Cronjé
 * @since 1.1
 */
public interface ExecMethods<E extends ExecutableExecSpec<E>> {

    /**
     * Creates an execution specification, configure it with the supplied configurator, then executes it.
     *
     * @param specConfigurator Specification configurator.
     * @return Execution result.
     */
    ExecResult exec(Action<E> specConfigurator);

    /**
     * Executes an existing execution specification.
     *
     * @param spec Specification to execute.
     * @return Execution result.
     */
    ExecResult exec(E spec);
}
