/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4.runnable;

import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.Internal;
import org.gradle.process.BaseExecSpec;
import org.gradle.process.ExecSpec;
import org.gradle.process.ProcessForkOptions;
import org.ysb33r.grolifant.api.core.ProjectOperations;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.ysb33r.grolifant.api.v4.MapUtils.stringizeValues;
import static org.ysb33r.grolifant.api.v4.StringUtils.stringize;

/**
 * @author Schalk W. Cronjé
 * @since 1.1
 */
public abstract class AbstractExecSpec<T extends ExecutableExecSpec<T>>
        implements ExecutableExecSpec<T>, CopyExecSpec {

    /**
     * Add an additional provider that will complement any values set via {@link #environment}.
     * <p>
     * When {@link #getEnvironmentProvider} is realized, additional providers will be called first, thus
     * values set via {@link #environment} will be definitive.
     *
     * @param envProvider
     * @since 1.2.0
     */
    @Override
    public void addEnvironmentProvider(Provider<Map<String, String>> envProvider) {
        this.envVarProviders.add(envProvider);
    }

    /**
     * Adds arguments for the executable to be executed.
     *
     * @param args Executable arguments
     * @return {@code this}
     */
    @Override
    public T exeArgs(Iterable<?> args) {
        for (Object i : args) {
            exeArgs.add(i);
        }
        return (T) this;
    }

    /**
     * Adds arguments for the executable to be executed.
     *
     * @param args Executable arguments
     * @return {@code this}
     */
    @Override
    public T exeArgs(Object... args) {
        for (Object i : args) {
            exeArgs.add(i);
        }
        return (T) this;
    }

    /**
     * Copies these options to the given target options.
     *
     * @param target Another implementation {@link ProcessForkOptions}.
     * @return {@code this}
     */
    @Override
    public T copyTo(ProcessForkOptions target) {
        target.setEnvironment(getEnvironment());
        target.setExecutable(getExecutable());
        target.setWorkingDir(getWorkingDir());
        return (T) this;
    }

    /**
     * Adds an environment variable to the environment for this process.
     *
     * @param name  Environment variable name
     * @param value Environment variable value
     * @return {@code this}
     */
    @Override
    public T environment(String name, Object value) {
        this.env.put(name, value);
        return (T) this;
    }

    /**
     * Adds some environment variables to the environment for this process.
     *
     * @param environmentVariables Map of environment variables
     * @return {@code this}
     */
    @Override
    public T environment(Map<String, ?> environmentVariables) {
        this.env.putAll(environmentVariables);
        return (T) this;
    }

    /**
     * Sets the name of the executable to use.
     *
     * @param executable Name of executable. This could be resolved to a {@link String}, {@link File}
     *                   or {@link Path} before execution
     * @return {@code this}
     */
    @Override
    public T executable(Object executable) {
        this.exe = executable;
        return (T) this;
    }

    /**
     * Returns the arguments for the executable to be executed.
     *
     * @return Arguments resolved to strings
     */
    @Override
    @Input
    public Provider<List<String>> getExeArgs() {
        return this.exeArgsProvider;
    }

    /**
     * Returns the full command line, including the executable plus its arguments.
     *
     * @return Command-line as separate elements in order. Never {@code null}, but list can be empty.
     */
    @Override
    @Internal
    public Provider<List<String>> getCommandLineProvider() {
        return this.commandLineProvider;
    }

    /**
     * The environment variables to use for the process.
     *
     * @return Environment, but not yet resolved from original values.
     */
    @Override
    @Internal
    public Map<String, Object> getEnvironment() {
        return this.env;
    }

    /**
     * The environment variables to use for the process.
     *
     * @return Provider to the environment, with values resovled to string
     */
    @Override
    @Input
    public Provider<Map<String, String>> getEnvironmentProvider() {
        return this.environmentProvider;
    }

    /**
     * Returns the output stream to consume standard error from the process executing the command.
     *
     * @return Error stream.
     */
    @Override
    @Internal
    public OutputStream getErrorOutput() {
        return this.stderr;
    }

    /**
     * Returns the name of the executable to use.
     *
     * @return Executable resolved to a string including the path if available.
     */
    @Override
    @Input
    public Provider<String> getExecutableProvider() {
        return this.executableProvider;
    }

    /**
     * Returns the standard input stream for the process executing the command.
     *
     * @return Standard input stream.
     */
    @Override
    @Internal
    public InputStream getStandardInput() {
        return this.stdin;
    }

    /**
     * Returns the output stream to consume standard output from the process executing the command.
     *
     * @return Standard output stream.
     */
    @Override
    @Internal
    public OutputStream getStandardOutput() {
        return this.stdout;
    }

    /**
     * Returns the working directory for the process.
     *
     * @return Provider to a working directory which was resolved to a {@link File}.
     */
    @Override
    @InputFile
    public Provider<File> getWorkingDirProvider() {
        return this.workingDirProvider;
    }

    /**
     * Tells whether a non-zero exit value is ignored, or an exception thrown.
     *
     * @return {@code true} if return code is ignored.
     */
    @Override
    @Internal
    public boolean isIgnoreExitValue() {
        return this.ignoreExit;
    }

    /**
     * Sets the arguments for the command to be executed.
     *
     * @param arguments Arguments can be resolved to string.
     * @return {@code this}.
     */
    @Override
    public T setExeArgs(Iterable<?> arguments) {
        this.exeArgs.clear();
        for (Object i : arguments) {
            this.exeArgs.add(i);
        }
        return (T) this;
    }

    /**
     * Sets the arguments for the command to be executed.
     *
     * @param arguments Arguments can be resolved to string.
     * @return {@code this}.
     */
    @Override
    public T setExeArgs(List<String> arguments) {
        this.exeArgs.clear();
        for (Object i : arguments) {
            this.exeArgs.add(i);
        }
        return (T) this;
    }

    /**
     * Replaces the existing environment variables to use for the process with a new collection.
     *
     * @param environmentVariables Environment variables
     */
    @Override
    public void setEnvironment(Map<String, ?> environmentVariables) {
        this.env.clear();
        this.env.putAll(environmentVariables);
    }

    /**
     * Sets the output stream to consume standard error from the process executing the command.
     *
     * @param outputStream Redirect error output to this stream.
     * @return {@code this}.
     */
    @Override
    public BaseExecSpec setErrorOutput(OutputStream outputStream) {
        this.stderr = outputStream;
        return (BaseExecSpec) this;
    }

    /**
     * Sets the name of the executable to use.
     * <p>
     * This will be resolved to a {@link File}, {@link String} or {@link Path} before execution.
     * </p>
     *
     * @param executable Executable, optionally including a path.
     */
    @Override
    public void setExecutable(Object executable) {
        this.exe = executable;
    }

    /**
     * Sets whether a non-zero exit value is ignored, or an exception thrown.
     *
     * @param ignoreExitValue {@code true} to prevent exception from being thrown on error.
     * @return {@code this}.
     */
    @Override
    public BaseExecSpec setIgnoreExitValue(boolean ignoreExitValue) {
        this.ignoreExit = ignoreExitValue;
        return (BaseExecSpec) this;
    }

    /**
     * Sets the standard input stream for the process executing the command.
     *
     * @param inputStream Redirect input to this stream.
     * @return {@code this}.
     */
    @Override
    public BaseExecSpec setStandardInput(InputStream inputStream) {
        this.stdin = inputStream;
        return (BaseExecSpec) this;
    }

    /**
     * Sets the output stream to consume standard output from the process executing the command.
     *
     * @param outputStream Redirect standard output to this stream.
     * @return {@code this}.
     */
    @Override
    public BaseExecSpec setStandardOutput(OutputStream outputStream) {
        this.stdout = outputStream;
        return (BaseExecSpec) this;
    }

    /**
     * Sets the working directory for the process.
     *
     * <p>Directory will be resolved to a {@link File} or {@link Path} before execution.</p>
     *
     * @param dir Working directory
     */
    @Override
    public void setWorkingDir(Object dir) {
        this.wd = dir;
    }

    @Override
    public void setExecutable(String s) {
        this.exe = s;
    }

    @Override
    public void setWorkingDir(File file) {
        this.wd = file;
    }

    @Override
    public ProcessForkOptions workingDir(Object o) {
        this.wd = o;
        return (ProcessForkOptions) this;
    }

    /**
     * Copies all values to a standard Gradle {@link ExecSpec}
     *
     * @param execSpec ExecSpec to copy to.
     */
    public void copyToExecSpec(ExecSpec execSpec) {
        copyTo(execSpec);
        execSpec.setCommandLine(buildCommandLine());
        execSpec.setErrorOutput(getErrorOutput());
        execSpec.setStandardInput(getStandardInput());
        execSpec.setStandardOutput(getStandardOutput());
        execSpec.setIgnoreExitValue(isIgnoreExitValue());
    }

    protected AbstractExecSpec(ProjectOperations projectOperations) {
        this.projectOperations = projectOperations;
        this.exeArgsProvider = projectOperations.provider(() -> stringize(exeArgs));
        this.commandLineProvider = projectOperations.provider(() -> buildCommandLine());
        this.executableProvider = projectOperations.provider(() -> stringize(exe));
        this.workingDirProvider = projectOperations.provider(() -> projectOperations.file(wd));
        this.stdin = System.in;
        this.stdout = System.out;
        this.stderr = System.err;
        this.wd = projectOperations.getProjectDir();

        this.environmentProvider = projectOperations.provider(() -> {
            Map<String, String> finalEnv = new TreeMap<>();
            envVarProviders.forEach(p -> finalEnv.putAll(p.get()));
            finalEnv.putAll(stringizeValues(env));
            return finalEnv;
        });
    }

    protected ProjectOperations getProjectOperations() {
        return this.projectOperations;
    }

    protected List<String> buildCommandLine() {
        List<String> args = new ArrayList<>();
        args.add(getExecutable());
        args.addAll(getExeArgs().get());
        return null;
    }

    private final ProjectOperations projectOperations;
    private final List<Object> exeArgs = new ArrayList<Object>();
    private final Map<String, Object> env = new TreeMap<String, Object>();
    private final List<Provider<Map<String, String>>> envVarProviders = new ArrayList();
    private final Provider<List<String>> exeArgsProvider;
    private final Provider<List<String>> commandLineProvider;
    private final Provider<Map<String, String>> environmentProvider;
    private final Provider<String> executableProvider;
    private final Provider<File> workingDirProvider;
    private InputStream stdin;
    private OutputStream stdout;
    private OutputStream stderr;
    private Object exe;
    private Object wd;
    private boolean ignoreExit = false;
}
