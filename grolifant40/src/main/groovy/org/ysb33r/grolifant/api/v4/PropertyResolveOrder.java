/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.v4;

import org.gradle.api.Project;
import org.gradle.api.provider.Provider;
import org.ysb33r.grolifant.api.core.ProjectOperations;
import org.ysb33r.grolifant.api.errors.UnsupportedConfigurationException;

import java.util.Map;

/** Resolves a property within a certain order
 *
 * @author Schalk W. Cronjé
 *
 * @since 0.15.0
 */
public interface PropertyResolveOrder {

    /** Resolves the property within the context of a Gradle project
     *
     * @param project Contextual project
     * @param name Project name
     * @return Resolved property value or {@code null}.
     *
     * @deprecated Use the version that returns a {@code Provider<String>} instead.
     */
    @Deprecated
    default String resolve(Project project, String name) {
        return resolve(ProjectOperations.find(project),name,true).getOrNull();
    }

    /** Resolves the property within the context of a Gradle project
     *
     * @param projectProps Project properties
     * @param name Project name
     * @return Resolved property value or {@code null}.
     *
     * @deprecated Use the version that returns a {@code Provider<String>} instead.
     */
    @Deprecated
   default String resolve(Map<String,?> projectProps, String name) {
        throw new UnsupportedConfigurationException(
                "Use resolve(ProjectOperations po, String name, boolean configurationTimeSafe) instead"
        );
    }

    /**
     * A provider to a property.
     *
     * @param po ProjectOperations
     * @param name Name of property
     * @param configurationTimeSafe Whether this property should be configuration time-safe.
     * @return Provider to property
     *
     * @since 1.1
     */
   Provider<String> resolve(ProjectOperations po, String name, boolean configurationTimeSafe);
}
