/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.loadable.v4

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.Transformer
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.ConfigurableFileTree
import org.gradle.api.file.CopySpec
import org.gradle.api.file.Directory
import org.gradle.api.file.FileCollection
import org.gradle.api.file.ProjectLayout
import org.gradle.api.internal.file.FileOperations
import org.gradle.api.internal.project.ProjectInternal
import org.gradle.api.internal.provider.MissingValueException
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.ysb33r.grolifant.api.core.ArchiveOperationsProxy
import org.ysb33r.grolifant.api.core.ExecOperationsProxy
import org.ysb33r.grolifant.api.core.GradleSysEnvProvider
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.core.ProviderTools
import org.ysb33r.grolifant.api.errors.NotSupportedException
import org.ysb33r.grolifant.api.v4.PropertyStore
import org.ysb33r.grolifant.api.v4.StringUtils
import org.ysb33r.grolifant.internal.v4.property.providers.GradleSysEnvProviderFactory
import org.ysb33r.grolifant.loadable.core.ProjectOperationsProxy

import java.lang.reflect.Method
import java.util.concurrent.Callable
import java.util.function.BiConsumer
import java.util.function.Function

import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_3
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_5_0
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_6_0
import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_6_6
import static org.ysb33r.grolifant.api.v4.FileUtils.fileize
import static org.ysb33r.grolifant.api.v4.FileUtils.updateFileProperty
import static org.ysb33r.grolifant.api.v4.StringUtils.stringize
import static org.ysb33r.grolifant.internal.core.ConfigurationCacheWarning.missingGrolifant50

/**
 * An extension that can be added to a project by a plugin to aid in compatibility
 *
 * @since 1.0.0
 */
@CompileStatic
@Slf4j
class DefaultProjectOperations extends ProjectOperationsProxy {

    /**
     * Constructor that sets up a number of methods to be compatible across a wide range Gradle releases.
     *
     * @param project
     */
    @SuppressWarnings(['MethodSize', 'DuplicateStringLiteral'])
    DefaultProjectOperations(Project project) {
        super(
            project,
            new DefaultFileSystemOperations(project),
            new DefaultTaskTools(project.tasks, project.providers)
        )
        projectCacheDir = project.gradle.startParameter.projectCacheDir ?: project.file("${project.rootDir}/.gradle")
        this.providerFactory = project.providers
        this.fileOperations = ((ProjectInternal) project).services.get(FileOperations)
        this.propertyProvider = GradleSysEnvProviderFactory.loadInstance(project, this)
        this.providerTools = new DefaultProviderTools(project.providers)

        if (PRE_6_0) {
            archives = new org.ysb33r.grolifant.internal.v4.DefaultArchiveOperations(project)
            execs = new org.ysb33r.grolifant.internal.v4.DefaultExecOperations(project)
            fileTreeFactory = { Object base -> project.fileTree(base) }
        } else {
            archives = new org.ysb33r.grolifant.internal.v6.DefaultArchiveOperations(project)
            execs = new org.ysb33r.grolifant.internal.v6.DefaultExecOperations(project)
            fileTreeFactory = v60FileTree(project.objects)
        }

        if (PRE_4_3) {
            this.updateStringProperty = { Provider<String> p, Object s ->
                if (p instanceof PropertyStore) {
                    StringUtils.updateStringProperty(project, p, s)
                } else {
                    throw new NotSupportedException(
                        'This method is not supported on Gradle <4.3 unless the Provider is a PropertyStore'
                    )
                }
            }
        } else {
            this.updateStringProperty = { ProjectOperations po, Provider<String> p, Object s ->
                StringUtils.updateStringProperty(po, p, s)
            }.curry(this) as BiConsumer<Provider<String>, Object>
        }
        if (PRE_4_3) {
            this.updateFileProperty = { Provider<File> p, Object f ->
                if (p instanceof PropertyStore) {
                    updateFileProperty(project, p, f)
                } else {
                    throw new NotSupportedException(
                        'This method is not supported on Gradle <4.3 unless the Provider is a PropertyStore'
                    )
                }
            }
        } else if (PRE_5_0) {
            this.files = { List<Object> f -> project.files(fileize(project, f)) }
            this.updateFileProperty = { Provider<File> p, Object f ->
                updateFileProperty(project, p, f)
            }
        } else {
            if (v5FileUtilsAvailable) {
                def functions = v5Functions(project)
                this.updateFileProperty = (BiConsumer) functions[1]
                this.files = (Function) functions[2]
            } else {
                if (!PRE_6_6) {
                    log.warn(missingGrolifant50())
                    log.warn('Falling back to 4.x behaviour')
                }
                this.files = { List<Object> f -> project.files(fileize(project, f)) }
                this.updateFileProperty = { Provider<File> p, Object f ->
                    updateFileProperty(project, p, f)
                }
            }
        }

        if (PRE_4_3) {
            this.buildDir = project.provider { -> project.buildDir }
        } else {
            def projectLayout = project.layout
            this.buildDir = map(projectLayout.buildDirectory, { Directory it ->
                it.asFile
            } as Transformer<File, Directory>)
        }

        if (PRE_6_6) {
            this.projectVersion = project.provider { -> stringize(project.version ?: '') }
            this.projectGroup = project.provider { -> stringize(project.group ?: '') }
        } else {
            Map extProps = project.extensions.extraProperties.properties
            this.projectVersion = project.provider { ->
                stringize(extProps['version'] ?: '')
            }
            this.projectGroup = project.provider { ->
                stringize(extProps['group'] ?: '')
            }
        }
    }

    @Override
    CopySpec copySpec() {
        fileOperations.copySpec()
    }

    /**
     * Converts a collection of file-like objects to a a list of  {@link java.io.File} instances with project context.
     * <p>
     * It will convert anything that the singular version of {@code FileUtils.fileize} can do.
     * In addition it will recursively resolve any collections that result out of resolving the supplied items.
     *
     * @param files List of object to evaluate
     * @return List of resolved files.
     */
    @Override
    List<File> fileize(Iterable<Object> files) {
        this.files.apply(files as List).files.toList()
    }

    /**
     * <p>Creates a {@link FileCollection} containing the given files, as defined by {@link Project#files(Object ...)}.
     *
     * <p>This method can also be used to create an empty collection, but the collection may not be mutated later.</p>
     *
     * @param paths The paths to the files. May be empty.
     * @return The file collection. Never returns null.
     */
    @Override
    FileCollection files(Object... paths) {
        this.files.apply(paths as List)
    }

    /**
     * Creates a new ConfigurableFileTree. The tree will have no base dir specified.
     *
     * @param base Base directory for file tree,
     * @return File tree.
     */
    @Override
    ConfigurableFileTree fileTree(Object base) {
        this.fileTreeFactory.apply(base)
    }

    /**
     * Build directory
     *
     * @return Provider to the build directory
     */
    @Override
    Provider<File> getBuildDir() {
        this.buildDir
    }

    /**
     * Lazy-evaluated project group.
     *
     * @return provider to project group
     */
    @Override
    Provider<String> getGroupProvider() {
        this.projectGroup
    }

    /**
     * Lazy-evaluated project version.
     *
     * @return Provider to project version
     */
    @Override
    Provider<String> getVersionProvider() {
        this.projectVersion
    }

    /**
     * Safely resolve the stringy items as a path below build directory.
     *
     * @param stringy Any item that can be resolved to a string using
     * {@link org.ysb33r.grolifant.api.v4.StringUtils#stringize}
     * @return Provider to a file
     */
    @Override
    Provider<File> buildDirDescendant(Object stringy) {
        map(this.buildDir, { File it ->
            new File(it, stringize(stringy))
        } as Transformer<File, File>)
    }

    /** Returns the project cache dir
     *
     * @return Location of cache directory
     */
    @Override
    File getProjectCacheDir() {
        this.projectCacheDir
    }

    /**
     * Tools to deal with provider down to Gradle 4.0.
     *
     * @return Tools instance optimised for current version of Gradle (where possible).
     */
    @Override
    ProviderTools getProviderTools() {
        this.providerTools
    }

    /** Returns a provider.
     *
     * @param var1 Anything that adheres to a Callable including Groovy closures or Java lambdas.
     * @return Provider instance.
     */
    @Override
    public <T> Provider<T> provider(Callable<? extends T> var1) {
        providerFactory.provider(var1)
    }

    /** Updates a file provider.
     *
     * Update property
     * otherwise the provider will be assigned a new Provider instance.
     *
     * @param provider Current property
     * @param stringy Value that should be lazy-resolved.
     */
    @Override
    void updateFileProperty(
        Provider<File> provider,
        Object file
    ) {
        this.updateFileProperty.accept(provider, file)
    }

    /**
     * Updates a string provider.
     * <p>
     * Update property or otherwise the provider will be assigned a new Provider instance.
     *
     * @param provider Current property
     * @param str Value that should be lazy-resolved to a string .
     */
    @Override
    void updateStringProperty(Provider<String> provider, Object str) {
        this.updateStringProperty.accept(provider, str)
    }

    @Override
    Provider<String> environmentVariable(Object name, boolean configurationTimeSafety) {
        propertyProvider.environmentVariable(name, configurationTimeSafety)
    }

    @Override
    Provider<String> gradleProperty(Object name, boolean configurationTimeSafety) {
        propertyProvider.gradleProperty(name, configurationTimeSafety)
    }

    @Override
    Provider<String> systemProperty(Object name, boolean configurationTimeSafety) {
        propertyProvider.systemProperty(name, configurationTimeSafety)
    }

    @Override
    protected ArchiveOperationsProxy getArchiveOperations() {
        this.archives
    }

    @Override
    protected ExecOperationsProxy getExecOperations() {
        this.execs
    }

    @Override
    protected GradleSysEnvProvider getPropertyProvider() {
        this.propertyProvider
    }

    @SuppressWarnings('DuplicateStringLiteral')
    private List<?> v5Functions(Project project) {
        def projectLayout = project.layout
        def providerFactory = project.providers
        Class fileUtils = DefaultProjectOperations.classLoader.loadClass(V5_FILEUTILS_CLASS)
        Method fileize = fileUtils.getMethod('fileize', ProjectLayout, Object)
        Method fileizeN = fileUtils.getMethod('fileize', ProjectLayout, Iterable)
        Method updateFileProperty = fileUtils.getMethod(
            'updateFileProperty',
            ProjectLayout,
            ProviderFactory,
            Property,
            Object
        )
        [
            { Object f ->
                (File) fileize.invoke(null, projectLayout, f)
            },
            { Provider<File> p, Object f ->
                updateFileProperty.invoke(null, projectLayout, providerFactory, (Property) p, f)
            },
            { List<Object> f ->
                projectLayout.files((List<File>) fileizeN.invoke(null, projectLayout, f))
            },
            { Object f ->
                try {
                    f == null ? null : (File) fileize.invoke(null, projectLayout, f)
                } catch (MissingValueException | NoSuchElementException e) {
                    null
                }
            }
        ]
    }

    private static boolean isV5FileUtilsAvailable() {
        try {
            DefaultProjectOperations.classLoader.loadClass(V5_FILEUTILS_CLASS)
            true
        } catch (ClassNotFoundException) {
            false
        }
    }

    @CompileDynamic
    private Function<Object, ConfigurableFileTree> v60FileTree(ObjectFactory objects) {
        { Object base -> objects.fileTree().from(base) } as Function<Object, ConfigurableFileTree>
    }

    private static final String V5_FILEUTILS_CLASS = 'org.ysb33r.grolifant.api.v5.FileUtils'

    private final Function<List<Object>, ConfigurableFileCollection> files
    private final Function<Object, ConfigurableFileTree> fileTreeFactory
    private final BiConsumer<Provider<File>, Object> updateFileProperty
    private final BiConsumer<Provider<String>, Object> updateStringProperty
    private final File projectCacheDir
    private final ProviderFactory providerFactory
    private final Provider<File> buildDir
    private final Provider<String> projectVersion
    private final Provider<String> projectGroup
    private final ArchiveOperationsProxy archives
    private final ExecOperationsProxy execs
    private final FileOperations fileOperations
    private final ProviderTools providerTools
    private final GradleSysEnvProvider propertyProvider
}
