/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.loadable.v4

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.Task
import org.gradle.api.provider.Provider
import org.gradle.api.provider.ProviderFactory
import org.gradle.api.tasks.TaskContainer
import org.ysb33r.grolifant.api.core.LegacyLevel
import org.ysb33r.grolifant.api.core.TaskTools

import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_4_9
import static org.ysb33r.grolifant.api.v4.TaskUtils.taskize

@CompileStatic
class DefaultTaskTools implements TaskTools {

    DefaultTaskTools(TaskContainer tasks, ProviderFactory pf) {
        this.tasks = tasks
        this.providerFactory = pf
    }

    /**
     * Registers a task in a lazy-manner.
     * <p>
     * On Gradle 4.9 or earlier it create the task.
     *
     * @param taskName Name of task to register. Task must have been registered or configured previously.
     * @param taskType Type of task.
     * @param configurator Configurating action.
     * @param <T >     Type of task.
     */
    @Override
    @CompileDynamic
    public <T extends DefaultTask> void register(String taskName, Class<T> taskType, Action<T> configurator) {
        if (PRE_4_9) {
            configurator.execute(tasks.create(taskName, taskType))
        } else {
            tasks.register(taskName, taskType).configure(configurator)
        }
    }

    /**
     * Configures a task, preferably in a lazy-manner.
     * <p>
     * On Gradle 4.9 or earlier it will use {@code getByName} internally.
     *
     * @param taskName Name of task to configure.  Task must have been registered or configured previously.
     * @param configurator Configurating action.
     */
    @Override
    @CompileDynamic
    void named(String taskName, Action<Task> configurator) {
        if (LegacyLevel.PRE_4_9) {
            configurator.execute(tasks.getByName(taskName))
        } else if (LegacyLevel.PRE_5_0) {
            tasks.named(taskName).configure(configurator)
        } else {
            tasks.named(taskName, configurator)
        }
    }

    /**
     * Adds a configuration for a task, for when a task is created.
     *
     * @param taskName Name of task to configure.
     * @param configurator Configurating action.
     */
    @Override
    void whenNamed(String taskName, Action<Task> configurator) {
        if (LegacyLevel.PRE_4_9) {
            tasks.whenTaskAdded { Task it ->
                if (it.name == taskName) {
                    configurator.execute(it)
                }
            }
        } else {
            tasks.configureEach { Task task ->
                if (task.name == taskName) {
                    configurator.execute(task)
                }
            }
        }
    }

    /**
     * Configures a task, preferably in a lazy-manner.
     * <p>
     * On Gradle 4.9 or earlier it will use {@code getByName} internally.
     *
     * @param taskName Name of task to configure. Task must have been registered or configured previously.
     * @param taskType Type of task.
     * @param configurator Configurating action.
     * @param <T >               Type of task.
     */
    @Override
    public <T extends DefaultTask> void named(String taskName, Class<T> taskType, Action<T> configurator) {
        if (LegacyLevel.PRE_4_9) {
            configurator.execute(taskType.cast(tasks.getByName(taskName)))
        } else if (LegacyLevel.PRE_5_0) {
            tasks.named(taskName).configure {
                configurator.execute(taskType.cast(it))
            }
        } else {
            tasks.named(taskName, taskType, configurator)
        }
    }

    /**
     * Resolves a maany to tasks items to a provider of tasks.
     *
     * @param taskies Things that can be converted to {@link Task} or {@link org.gradle.api.tasks.TaskProvider}
     * @return A provider to the list of resolved tasks.
     */
    @Override
    Provider<List<? extends Task>> taskize(Object... taskies) {
        taskize(taskies as List)
    }

    /**
     * Resolves a maany to tasks items to a provider of tasks.
     *
     * @param taskies Things that can be converted to {@link Task} or {@link org.gradle.api.tasks.TaskProvider}
     * @return A provider to the list of resolved tasks.
     */
    @Override
    Provider<List<? extends Task>> taskize(Iterable<Object> taskies) {
        providerFactory.provider { ->
            taskize(tasks, taskies) as List
        }
    }

    /**
     * Adds a configuration for a task, for when a task is created.
     *
     * @param taskName Name of task to configure.
     * @param taskType Type of task.
     * @param configurator Configurating action.
     * @param <T >                  Type of task.
     */
    @Override
    @CompileDynamic
    public <T extends DefaultTask> void whenNamed(String taskName, Class<T> taskType, Action<T> configurator) {
        if (LegacyLevel.PRE_4_9) {
            tasks.whenTaskAdded { Task it ->
                if (it.name == taskName) {
                    configurator.execute(it)
                }
            }
        } else {
            tasks.withType(taskType) { T task ->
                if (task.name == taskName) {
                    configurator.execute(task)
                }
            }
        }
    }

    private final TaskContainer tasks
    private final ProviderFactory providerFactory
}
