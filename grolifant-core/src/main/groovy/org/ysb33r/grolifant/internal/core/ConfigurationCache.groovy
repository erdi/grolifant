/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.internal.core

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.internal.StartParameterInternal

import static org.ysb33r.grolifant.api.core.LegacyLevel.PRE_6_6

/**
 * Checks where configuration cache is enabled.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.1
 */
@CompileStatic
class ConfigurationCache {
    static boolean isEnabled(Project project) {
        PRE_6_6 ? false : checkStartParameter(project)
    }

    @CompileDynamic
    private static boolean checkStartParameter(Project project) {
        ((StartParameterInternal) project.gradle.startParameter).configurationCache
    }
}
