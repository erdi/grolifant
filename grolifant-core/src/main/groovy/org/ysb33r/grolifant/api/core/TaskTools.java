/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.core;

import org.gradle.api.Action;
import org.gradle.api.DefaultTask;
import org.gradle.api.Task;
import org.gradle.api.provider.Provider;

import java.util.List;

/**
 * Utilities for working with tasks in a consistent manner across Gradle versions.
 *
 * @author Schalk W. Cronjé
 *
 * @since 1.3
 */
public interface TaskTools {
    /**
     * Configures a task, preferably in a lazy-manner.
     * <p>
     * On Gradle 4.9 or earlier it will use {@code getByName} internally.
     *
     * @param taskName     Name of task to configure.  Task must have been registered or configured previously.
     * @param configurator Configurating action.
     */
    void named(String taskName, Action<Task> configurator);

    /**
     * Configures a task, preferably in a lazy-manner.
     * <p>
     * On Gradle 4.9 or earlier it will use {@code getByName} internally.
     *
     * @param taskName     Name of task to configure. Task must have been registered or configured previously.
     * @param taskType     Type of task.
     * @param configurator Configurating action.
     * @param <T>          Type of task.
     */
    <T extends DefaultTask> void named(String taskName, Class<T> taskType, Action<T> configurator);

    /**
     * Registers a task in a lazy-manner.
     * <p>
     * On Gradle 4.9 or earlier it create the task.
     *
     * @param taskName     Name of task to register. Task must have been registered or configured previously.
     * @param taskType     Type of task.
     * @param configurator Configurating action.
     * @param <T>          Type of task.
     */
    <T extends DefaultTask> void register(String taskName, Class<T> taskType, Action<T> configurator);

    /**
     * Resolves a maany to tasks items to a provider of tasks.
     *
     * @param taskies Things that can be converted to {@link Task} or {@link org.gradle.api.tasks.TaskProvider}
     * @return A provider to the list of resolved tasks.
     */
    Provider<List<? extends Task>> taskize(Object... taskies);

    /**
     * Resolves a maany to tasks items to a provider of tasks.
     *
     * @param taskies Things that can be converted to {@link Task} or {@link org.gradle.api.tasks.TaskProvider}
     * @return A provider to the list of resolved tasks.
     */
    Provider<List<? extends Task>> taskize(Iterable<Object> taskies);

    /**
     * Adds a configuration for a task, for when a task is created.
     *
     * @param taskName     Name of task to configure.
     * @param configurator Configurating action.
     */
    void whenNamed(String taskName, Action<Task> configurator);

    /**
     * Adds a configuration for a task, for when a task is created.
     *
     * @param taskName     Name of task to configure.
     * @param taskType     Type of task.
     * @param configurator Configurating action.
     * @param <T>          Type of task.
     */
    <T extends DefaultTask> void whenNamed(String taskName, Class<T> taskType, Action<T> configurator);
}
