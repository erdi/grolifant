/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.core;

import org.gradle.api.Action;
import org.gradle.api.file.CopySpec;
import org.gradle.api.file.DeleteSpec;
import org.gradle.api.tasks.WorkResult;

import java.io.File;

/**
 * Abstracts a number of file operations.
 *
 * Also Acts as a proxy for the {@code FileSystemOperations} that was implemented in Gradle 6.0,
 * but allows for a substitute operation on old Gradle versions.
 *
 * @since 1.3
 */
public interface FileSystemOperations {
    /**
     * Copies the specified files.
     * @param action Configures a {@link CopySpec}
     * @return Result of copy to check whether it was successful.
     */
    WorkResult copy(Action<? super CopySpec> action);

    /**
     * Deletes the specified files.
     * @param action Configures a {@link DeleteSpec}
     * @return  Result of deletion to check whether it was successful.
     */
    WorkResult delete(Action<? super DeleteSpec> action);

    /**
     * Converts a file-like object to a {@link java.io.File} instance with project context.
     * <p>
     * Converts any of the following recursively until it gets to a file:
     *
     * <ul>
     *   <li> {@code CharSequence} including {@code String} and {@code GString}.
     *   <li> {@link java.io.File}.
     *   <li> {@link java.nio.file.Path} is it is associated with the default provider
     *   <li> URLs and URis of {@code file:} schemes.
     *   <li> Groovy Closures.
     *   <li> {@link java.util.concurrent.Callable}.
     *   <li> {@link org.gradle.api.provider.Provider}.
     *   <li> {@link org.gradle.api.file.Directory} (Gradle 4.1+)
     *   <li> {@link org.gradle.api.resources.TextResource}
     * </ul>
     *
     * @param file Potential {@link File} object
     * @return File instance.
     */
    File file(Object file);

    /**
     * Similar to {@Link #file}, but does not throw an exception if the object is {@code null} or an empty provider.
     *
     * @param file Potential {@link File} object
     * @return File instance or {@code null}.
     */
    File fileOrNull(Object file);

    /**
     * Returns the relative path from the project directory to the given path.
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     */
    String relativePath(Object f);

    /**
     * Returns the relative path from the root project directory to the given path.
     *
     * @param f  Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     */
    String relativeRootPath(Object f);

    /**
     * Returns the relative path from the given path to the project directory.
     *
     * @param f Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     */
    String relativePathToProjectDir(Object f);

    /**
     * Returns the relative path from the given path to the root project directory.
     *
     * @param f  Object that is resolvable to a file within project context
     * @return Relative path. Never {@code null}.
     */
    String relativePathToRootDir(Object f);

    /**
     * Synchronizes the contents of a destination directory with some source directories and files.
     *
     * @param action Action to configure the CopySpec.
     * @return {@link WorkResult} that can be used to check if the sync did any work.
     */
    WorkResult sync(Action<? super CopySpec> action);
}
