/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api.artifacts

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.grolifant.api.core.CheckSumVerification

/** A custom implementation of a dependency cache when the one from Gradle
 * might not be directly accessible.
 *
 * @deprecated Use{@link org.ysb33r.grolifant.api.v4.artifacts.DependencyCache} instead.
 * @since 0.10
 */
@CompileStatic
@Deprecated
@SuppressWarnings(['ClassNameSameAsSuperclass', 'LineLength'])
class DependencyCache extends org.ysb33r.grolifant.api.v4.artifacts.DependencyCache {

    /**
     * @deprecated Use{@link org.ysb33r.grolifant.api.v4.artifacts.DependencyCache.PostDownloadProcessor} instead.
     */
    static interface PostDownloadProcessor extends org.ysb33r.grolifant.api.v4.artifacts.DependencyCache.PostDownloadProcessor {
    }

    /**
     * @deprecated Use{@link org.ysb33r.grolifant.api.v4.artifacts.DependencyCache.CacheRefresh} instead.
     */
    static interface CacheRefresh extends org.ysb33r.grolifant.api.v4.artifacts.DependencyCache.CacheRefresh {
    }

    @SuppressWarnings('ParameterCount')
    DependencyCache(
        final Project project,
        final String cacheName,
        final String cacheRelativePath,
        final CacheRefresh requiresDownload,
        final CheckSumVerification checksumVerification,
        final PostDownloadProcessor postprocessor
    ) {
        super(project, cacheName, cacheRelativePath, requiresDownload, checksumVerification, postprocessor)
    }
}
