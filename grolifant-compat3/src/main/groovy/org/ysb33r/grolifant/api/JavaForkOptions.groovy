/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 * ============================================================================
 */
package org.ysb33r.grolifant.api

import groovy.transform.CompileStatic
import groovy.transform.InheritConstructors

/** Provides a class that can be populated with various fork options for Java
 * and which can then be used to copy to other methods in the Gradle API that provides a
 * {@link org.gradle.process.JavaForkOptions} in the parameters.
 *
 * @deprecated Use{@link org.ysb33r.grolifant.api.v4.JavaForkOptions} instead.
 *
 * @since 0.7
 */
@CompileStatic
@SuppressWarnings(['MethodCount', 'ClassNameSameAsSuperclass'])
@Deprecated
@InheritConstructors
class JavaForkOptions extends org.ysb33r.grolifant.api.v4.JavaForkOptions {
}
